package com.kodebonek.instatraffic.art;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kodebonek.instatraffic.utils.LocationData;
import com.kodebonek.instatraffic.MainActivity;
import com.kodebonek.instatraffic.R;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by bayufa on 6/21/17.
 */

public class ArtFragment extends Fragment {

    private final String TAG = "ArtFragment";
    View rootView = null;
    int pos = 0;
    private List<String> trafficLevels;

    public static ArtFragment newInstance(Context context, int position) {
        ArtFragment fragment = new ArtFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        fragment.setArguments(args);
        return(fragment);
    }

    public ArtFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        trafficLevels = Arrays.asList(getActivity().getResources().getStringArray(R.array.traffic_level));
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
/*
            if (isVisibleToUser && getView()!=null) {
                //Log.d(TAG,"setUserVisibleHint()");
                //view already created..just fill the datas
                updateWidgetText(rootView, mLocationData);
                updateBoxTransparency(rootView);
                updateTrafficStatus(rootView);
            }
*/
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle args = getArguments();
        int position = args.getInt("position");
        pos = position;

        String layoutname = "template_basic_"+position;
        int layoutID = getResources().getIdentifier(layoutname, "layout", getActivity().getPackageName());
        if (layoutID==0) {
            Log.e(TAG, "invalid layoutID : " + layoutname);
            return(null);
        } else Log.d(TAG,"layout "+layoutname+" inflated");

        rootView = inflater.inflate(layoutID, container, false);

        updateWidgetText(rootView, ArtData.getInstance().location);
        updateBoxTransparency(rootView);
        updateTrafficStatus(rootView);

        View boxView = (View) rootView.findViewById(R.id.boxContainer);
        boxView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showCustomizeDialog();
                ArtData.getInstance().showTransparentBox = !ArtData.getInstance().showTransparentBox;
                updateBoxTransparency(rootView);
                ((MainActivity) getActivity()).broadcastUpdateToFragments();
            }
        });

        View tvStatus = (View) rootView.findViewById(R.id.tvStatus);
        if (tvStatus!=null)
            tvStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ArtData.getInstance().currentTrafficLevel++;
                    updateTrafficStatus(rootView);
                    ((MainActivity) getActivity()).broadcastUpdateToFragments();
                }
            });

        return rootView;
    }

    /*
    private void showCustomizeDialog() {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.customize_dialog, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setView(layout);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setTitle("Customize Text");
        alertDialog.show();
    }
    */

    private void updateTrafficStatus(View rootView) {
        if (rootView==null) return;

        TextView tvStatus = (TextView) rootView.findViewById(R.id.tvStatus);
        if (tvStatus==null) {
            Log.e(TAG,"updateTrafficStatus() tvStatus null??");
            return;
        }

        if (ArtData.getInstance().currentTrafficLevel>=trafficLevels.size())
            ArtData.getInstance().currentTrafficLevel=0;
        String level = trafficLevels.get(ArtData.getInstance().currentTrafficLevel);
        tvStatus.setText(level);
    }

    private void updateBoxTransparency(View rootView) {
        if (rootView==null) return;

        View boxView = (View) rootView.findViewById(R.id.boxContainer);
        if (boxView==null) {
            Log.e(TAG,"updateBoxTransparency() boxView null??");
            return;
        }
        if (ArtData.getInstance().showTransparentBox)
            boxView.setBackgroundColor(getResources().getColor(R.color.app_box_background));
        else
            boxView.setBackgroundColor(getResources().getColor(android.R.color.transparent));
    }

    private void updateWidgetText(View rootView,LocationData loc) {
        if (rootView==null) return;

        TextView tvDate = (TextView) rootView.findViewById(R.id.tvDate);
        if (tvDate!=null) {
            String date = new SimpleDateFormat("d MMM yyyy HH:mm", Locale.ENGLISH).format(new Date());
            tvDate.setText(date);
        }

        TextView tvStreet = (TextView) rootView.findViewById(R.id.tvStreet);
        if (tvStreet!=null) {
            tvStreet.setText(loc.street);
        }

        TextView tvCity = (TextView) rootView.findViewById(R.id.tvCity);
        if (tvCity!=null) {
            tvCity.setText(loc.city);
        }

        TextView tvStreetCity = (TextView) rootView.findViewById(R.id.tvStreetCity);
        if (tvStreetCity!=null) {
            String street_city = loc.street+" , "+loc.city;
            tvStreetCity.setText(street_city);
        }

        TextView tvCityCountry = (TextView) rootView.findViewById(R.id.tvCityCountry);
        if (tvCityCountry!=null) {
            String city_country = loc.city+" , "+loc.country_code;
            tvCityCountry.setText(city_country);
        }
    }

    public void updateInfo(LocationData loc) {
        //((TextView) rootView.findViewById(R.id.tvStreet)).setText(loc.street);
        if (getView()==null) return;

        updateWidgetText(getView(), loc);
    }

    public void updateInfo() {
        //((TextView) rootView.findViewById(R.id.tvStreet)).setText(loc.street);
        if (getView()==null) return;

        updateBoxTransparency(getView());
        updateTrafficStatus(getView());
    }
}