package com.kodebonek.instatraffic.art;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kodebonek.instatraffic.utils.LocationData;

public class ArtPagerAdapter extends FragmentPagerAdapter {

    private static final String TAG = "ArtPagerAdapter";
    private int NUM_TEMPLATE = 9;
    private Context mContext;

    public ArtPagerAdapter(FragmentManager fm,Context ctx) {
        super(fm);
        mContext = ctx;
    }

    /*
    public void setContext(Context ctx) {
        mContext = ctx;
    }
    */

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = ArtFragment.newInstance(mContext, position + 1);
        return(fragment);
    }

    @Override
    public int getCount() {
        return (NUM_TEMPLATE);
    }

}
