package com.kodebonek.instatraffic.art;

import com.kodebonek.instatraffic.utils.LocationData;

/**
 * Created by bayufa on 6/21/17.
 */

public class ArtData {

    private static ArtData instance;

    public LocationData location = null;
    public boolean showTransparentBox = true;
    public int currentTrafficLevel = 2;   //heavy traffic
    //public List<String> trafficLevels;

    public static ArtData getInstance() {
        if (instance==null)
            instance = new ArtData();
        return instance;
    }

    ArtData() {
        location = new LocationData();
        location.country = "...";
        location.street = "...";
        location.country_code = "...";
        location.city = "...";
        location.province = "...";
        //trafficLevels = Arrays.asList(ctx.getResources().getStringArray(R.array.traffic_level));
    }
}
