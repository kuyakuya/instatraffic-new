package com.kodebonek.instatraffic.timeline;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.kodebonek.instatraffic.R;
import com.kodebonek.instatraffic.timeline.TimelineActivity;
import com.kodebonek.instatraffic.timeline.ViewPhotoActivity;
import com.kodebonek.instatraffic.utils.MyHelper;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GridAdapter extends BaseAdapter {

    private static final String TAG = "GridAdapter";
    JSONArray mJsonArray;
    private Context mContext;
    private Location mLocation;

    public GridAdapter(Context context,Location loc) {
        mContext = context;
        mJsonArray = new JSONArray();
        mLocation = loc;
    }

    @Override
    public int getCount() {
        if (mJsonArray==null) return 0;

        return mJsonArray.length();
    }

    @Override
    public Object getItem(int position) {
        if (mJsonArray==null) return null;

        JSONObject dataObj = mJsonArray.optJSONObject(position);
        return dataObj;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void updateData(JSONArray arr) {
        if (arr==null) return;
        mJsonArray = arr;
        preCalculateDistance();
    }

    public void appendData(JSONArray arr) {

        try {
            mJsonArray = concatArray(mJsonArray,arr);
            preCalculateDistance();
            Log.d(TAG,"appendData. total now:"+mJsonArray.length());
        } catch (JSONException e) {
            Log.d(TAG, e.toString());
            return;
        }
    }

    private void preCalculateDistance() {

        String distance,metric;
        Location tmp = new Location("");
        for (int i=0; i < mJsonArray.length(); i++) {
            JSONObject obj = mJsonArray.optJSONObject(i);
            if (obj==null) continue;

            tmp.setLatitude(obj.optDouble("latitude", 0));
            tmp.setLongitude(obj.optDouble("longitude", 0));
/*
            float distanceInMeters = mLocation.distanceTo(tmp);
            if (distanceInMeters<1000)
                distance = (int) distanceInMeters + " m";
            else if (distanceInMeters > 100000)
                distance = "> 100 km";
            else
                distance = (int) (distanceInMeters/1000) + " km";
*/
            metric = obj.optString("metric","km");
            distance = String.valueOf(obj.optDouble("distance",0))+" "+metric;

            try {
                obj.put("distance", distance);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private JSONArray concatArray(JSONArray arr1, JSONArray arr2)
            throws JSONException {
        JSONArray result = new JSONArray();
        for (int i = 0; i < arr1.length(); i++) {
            result.put(arr1.get(i));
        }
        for (int i = 0; i < arr2.length(); i++) {
            result.put(arr2.get(i));
        }
        return result;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final JSONObject dataObj = mJsonArray.optJSONObject(position);
        if (dataObj==null) {
            return convertView;
        }
        final String imageURL = dataObj.optString("media");
        final String thumbURL = dataObj.optString("thumb");
        final String created = dataObj.optString("created");
        final String date = dataObj.optString("date");
        final String distance = dataObj.optString("distance");;
        final String displayname = dataObj.optString("displayname");;
        final String comment = dataObj.optString("comment");;

        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.grid_item, parent, false);

            holder = new ViewHolder();
            holder.position = (Button) convertView.findViewById(R.id.btnPosition);
            holder.image = (ImageView) convertView.findViewById(R.id.image);
            holder.time = (TextView) convertView.findViewById(R.id.tvTime);
            holder.distance = (TextView) convertView.findViewById(R.id.tvDistance);
            holder.goButton = (ImageButton) convertView.findViewById(R.id.btnGo);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.position.setText(MyHelper.toAlphabetic(position));

        final TimelineActivity activity = (TimelineActivity) mContext;
        final double lat = dataObj.optDouble("latitude",0);
        final double lon = dataObj.optDouble("longitude",0);

        //TODO: should move this listener to holder pattern..

        holder.position.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.moveCamera(new LatLng(lat,lon));
            }
        });

        holder.goButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.moveCamera(new LatLng(lat,lon));
            }
        });

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext,"Load the fuckin image of "+imageURL,Toast.LENGTH_SHORT).show();
                Intent myIntent = new Intent(mContext, ViewPhotoActivity.class);
                myIntent.putExtra("date", date);
                myIntent.putExtra("distance", distance);
                myIntent.putExtra("url", imageURL);
                myIntent.putExtra("displayname", displayname);
                myIntent.putExtra("comment", comment);
                mContext.startActivity(myIntent);
            }
        });

        //Log.d(TAG,"Loading "+imageURL);
        Picasso.with(mContext)
                .load(thumbURL)
                .placeholder(R.drawable.ic_placeholder)
                //.resize(200,200)
                .into(holder.image, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError() {
                        Log.d(TAG, "Load image " + imageURL + " error");
                    }
                });


        holder.time.setText(created);
        holder.distance.setText(distance);

        return convertView;
    }

    public void clear() {
        if (mJsonArray==null) return;

        mJsonArray=new JSONArray();
    }

    class ViewHolder {
        public Button position;
        public ImageView image;
        public TextView time;
        public TextView distance;
        public ImageButton goButton;
    }

}
