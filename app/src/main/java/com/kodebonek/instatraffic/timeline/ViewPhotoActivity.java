package com.kodebonek.instatraffic.timeline;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.kodebonek.instatraffic.R;
import com.kodebonek.instatraffic.utils.MyHelper;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;


public class ViewPhotoActivity extends Activity {

    private static final String TAG = "ViewPhotoActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        //http://stackoverflow.com/a/28734909/4772917
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_view_photo);
        Intent intent = getIntent();
        final String imageURL = intent.getStringExtra("url");
        String date = intent.getStringExtra("date");
        String distance = intent.getStringExtra("distance");
        String displayname = intent.getStringExtra("displayname");
        if (displayname.length()==0) displayname = "Anonymous";
        String comment = intent.getStringExtra("comment");
        if (comment.length()==0) comment = "<empty>";

        this.setFinishOnTouchOutside(true);

        Log.d(TAG, "onCreate - url:" + imageURL);

        ((TextView) findViewById(R.id.tvDate)).setText(date);
        ((TextView) findViewById(R.id.tvDistance)).setText(distance);
        ((TextView) findViewById(R.id.tvDisplayName)).setText("by "+displayname);
        ((TextView) findViewById(R.id.tvComment)).setText(comment);

        int width = MyHelper.getDeviceWidth(getApplicationContext());
        if (width>900) width = 900;

        ImageView iv = (ImageView) findViewById(R.id.imageView);
        iv.setMinimumWidth(width);
        iv.setMinimumHeight(width);
        Picasso.with(getParent())
                .load(imageURL)
                .placeholder(R.drawable.ic_placeholder)
                .resize(width,width)
                .into(iv, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError() {
                        Log.d(TAG, "Load image " + imageURL + " error");
                    }
                });

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,"onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,"onResume");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"onDestroy");
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
