package com.kodebonek.instatraffic.timeline;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.kodebonek.instatraffic.R;
import com.kodebonek.instatraffic.utils.MyHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class TimelineActivity extends ActionBarActivity {

    private static final String TAG = "TimelineActivity";
    public static final String KEY_SEARCH_RADIUS = "search_radius";
    private GridAdapter mGridAdapter;
    private GridView mGridView;
    Location currentLocation;
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private ClusterManager<MyItem> mClusterManager;
    private IconGenerator mIconGenerator;
    //private String[] radius_settings;
    private List<String> radius_settings;
    private int mSearchRadius = 0;
    private int currentPage = 0;
    private boolean loadingInProgress = false;
    private boolean stopLoadingData = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);
        Intent intent = getIntent();
        currentLocation = new Location("");
        currentLocation.setLatitude(intent.getDoubleExtra("latitude", 0));
        currentLocation.setLongitude(intent.getDoubleExtra("longitude", 0));
        Log.d(TAG, "currentLocation : " + currentLocation.toString());

        //load settings and values
        radius_settings = Arrays.asList(getResources().getStringArray(R.array.distance_setting_value));
        currentPage = 0;
        loadingInProgress = false;
        stopLoadingData = false;

        mSearchRadius = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt(KEY_SEARCH_RADIUS,1);
        Log.d(TAG,"search_radius settings: "+mSearchRadius);

        Toolbar actionBar = (Toolbar) findViewById(R.id.toolbarTop);
        setSupportActionBar(actionBar);

        setupGrid();

        ImageView backButton = (ImageView) findViewById(R.id.ivBack);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ImageView settingButton = (ImageView) findViewById(R.id.ivDistanceSetting);
        settingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDistanceSetting();
            }
        });

        setUpMapIfNeeded();
        queryServer();
    }

    private void setupGrid() {

        mGridAdapter = new GridAdapter(TimelineActivity.this,currentLocation);
        mGridView = (GridView) findViewById(R.id.grid);
        mGridView.setAdapter(mGridAdapter);

        loadingInProgress = true;   //IMPORTANT - flag it , so that it wont double loading on startup
        mGridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == totalItemCount) && !(loadingInProgress)) {
                    if (stopLoadingData == false) {
                        // FETCH THE NEXT BATCH OF FEEDS
                        currentPage++;
                        Log.d(TAG,"reload more data - page:"+currentPage);
                        queryServer();
                    }
                }
            }
        });

    }

    private void showDistanceSetting() {

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.distance_dialog, (ViewGroup) findViewById(R.id.distance_setting_layout));

        AlertDialog.Builder builder = new AlertDialog.Builder(this).setView(layout);
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //save settings
                SeekBar sb = (SeekBar) layout.findViewById(R.id.seekBar);
                if (sb.getProgress()<radius_settings.size()) {
                    mSearchRadius = Integer.valueOf(radius_settings.get(sb.getProgress()));
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
                            .edit().putInt(KEY_SEARCH_RADIUS, mSearchRadius).commit();

                    stopLoadingData = false;
                    currentPage = 0;
                    queryServer();
                }
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setTitle("Set search radius");
        //alertDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        alertDialog.show();

        final TextView tv = (TextView) layout.findViewById(R.id.tvDistanceValue);
        SeekBar sb = (SeekBar)layout.findViewById(R.id.seekBar);
        sb.setMax(radius_settings.size()-1);
        int pos = radius_settings.indexOf(String.valueOf(mSearchRadius));
        if (pos<0) pos = 1;
        sb.setProgress(pos);
        tv.setText(mSearchRadius+" Km");
        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress > radius_settings.size() - 1) return;

                int radius = Integer.valueOf(radius_settings.get(progress));
                tv.setText(radius + " Km");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }



    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapFragment))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                mIconGenerator = new IconGenerator(TimelineActivity.this);
                //mIconGenerator.setContentPadding(10,2,10,2);
                setupClusterManager();
                setUpMap();
            } else {
                Toast.makeText(getApplicationContext(),"Initialize map fail. Do your phone support Google Map?",Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setupClusterManager() {

        mClusterManager = new ClusterManager<MyItem>(TimelineActivity.this,mMap);
        mClusterManager.setRenderer(new MyClusterRenderer(TimelineActivity.this, mMap, mClusterManager));
        mClusterManager.setOnClusterClickListener(new ClusterManager.OnClusterClickListener<MyItem>() {
            @Override
            public boolean onClusterClick(Cluster<MyItem> cluster) {
                int size = cluster.getItems().size();
                //Toast.makeText(getApplicationContext(),"onClusterClick size:"+size,Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        //mClusterManager.setOnClusterInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<MyItem>() {
            @Override
            public boolean onClusterItemClick(MyItem myItem) {
                //Toast.makeText(getApplicationContext(),"onClusterItemClick pos:"+myItem.position,Toast.LENGTH_SHORT).show();
                moveCamera(myItem.location);
                return true;
            }
        });
        //mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        mMap.setOnCameraChangeListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);
    }

    public void moveCamera(LatLng location) {
        mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
    }

    private void setUpMap() {
        LatLng myLoc = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());

        //mMap.addMarker(new MarkerOptions().position(myLoc).title("You"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLoc, 12));
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        MyItem item = new MyItem(currentLocation.getLatitude(), currentLocation.getLongitude(),0,true);
        mClusterManager.addItem(item);
    }

    private void queryServer() {

        loadingInProgress = true;
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(60000);
        client.setMaxRetriesAndTimeout(2, 500);

        double lat = currentLocation.getLatitude();
        double lon = currentLocation.getLongitude();
        String rnd = String.valueOf(lat * lon * mSearchRadius).substring(0,5);

        String URL = "http://api.kodebonek.com/instatraffic/near.php?key="+ MyHelper.generateAPIKey(rnd)+"&lat="+lat+"&lon="+lon+"" +
                "&radius="+mSearchRadius+"&metric=km&page="+currentPage;
        Log.d(TAG, URL);
        client.get(URL, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                showProgress(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                //Log.d(TAG,"response: "+response.toString());
                if (response.has("success")) {
                    int success = response.optInt("success");
                    if (success==1) {

                        JSONArray data = response.optJSONArray("data");
                        Log.d(TAG, "jsonData (count:" + data.length() + ") : " + data.toString());

                        //reset current data , if it starting point
                        if (currentPage==0) {
                            mGridAdapter.clear();
                            mClusterManager.clearItems();
                        }

                        if (data.length()>0) {

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject obj = data.optJSONObject(i);
                                if (obj == null) continue;

                                MyItem item = new MyItem(obj.optDouble("latitude", 0), obj.optDouble("longitude", 0), i, false);
                                mClusterManager.addItem(item);
                            }

                            if (mGridAdapter.getCount()==0)
                                mGridAdapter.updateData(data);  //first time
                            else
                                mGridAdapter.appendData(data);

                        } else {
                            //ehh no more data ???
                            Log.d(TAG,"No more data from the server");
                            if (mGridAdapter.getCount()>6)  //max visible grid
                                Toast.makeText(getApplicationContext(),"No more data",Toast.LENGTH_SHORT).show();
                            stopLoadingData = true; //set it..but reset it later if user choose to pull to refresh again
                        }

                        //start the Fckin cluster
                        mGridAdapter.notifyDataSetChanged();
                        mClusterManager.cluster();

                    } else {
                        String message = response.optString("message");
                        Log.e(TAG, "json response failed: " + message);
                        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.e(TAG, "invalid api response. dump: " + response.toString());
                    Toast.makeText(getApplicationContext(),"invalid response from server",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                //super.onFailure(statusCode, headers, responseString, throwable);
                Toast.makeText(getApplicationContext(),"Fail waiting reponse, please try again ("+statusCode+" - "+responseString+")", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "onFailure: "+responseString+" ("+statusCode+")");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(getApplicationContext(), "Unable to connect to server", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "onFailure2: " + throwable.getMessage()+" code:"+statusCode);
            }

            @Override
            public void onFinish() {
                showProgress(false);
                loadingInProgress = false;
            }
        });
    }

    private void showProgress(boolean show) {
        ProgressBar pb = (ProgressBar) findViewById(R.id.progressBar);
        if (show) {
            pb.bringToFront();
            pb.setVisibility(View.VISIBLE);
        } else
            pb.setVisibility(View.INVISIBLE);
    }

    class MyItem implements ClusterItem {
        public final int position;
        private final LatLng location;
        private final boolean startingPoint;

        public MyItem(double lat, double lng, int pos,boolean current) {
            location = new LatLng(lat, lng);
            position = pos;
            this.startingPoint = current;
        }

        @Override
        public LatLng getPosition() {
            return location;
        }
    }

    class MyClusterRenderer extends DefaultClusterRenderer<MyItem> {

        public MyClusterRenderer(Context context, GoogleMap map, ClusterManager<MyItem> clusterManager) {
            super(context, map, clusterManager);
        }

        @Override
        protected void onBeforeClusterItemRendered(MyItem item, MarkerOptions markerOptions) {
            //super.onBeforeClusterItemRendered(item, markerOptions);
            //Log.d(TAG,"onBeforeClusterItemRendered");
            Bitmap icon = null;
            if (item.startingPoint) {
                mIconGenerator.setStyle(IconGenerator.STYLE_GREEN);
                icon = mIconGenerator.makeIcon("You");

            } else {
                mIconGenerator.setStyle(IconGenerator.STYLE_ORANGE);
                icon = mIconGenerator.makeIcon(MyHelper.toAlphabetic(item.position));
            }

            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<MyItem> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
            //Log.d(TAG,"onBeforeClusterRendered");
            super.onBeforeClusterRendered(cluster, markerOptions);
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster<MyItem> cluster) {
            return (cluster.getSize()>5);
        }
    }

}
