package com.kodebonek.instatraffic;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.location.Address;
import android.location.Location;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kodebonek.instatraffic.art.ArtData;
import com.kodebonek.instatraffic.art.ArtFragment;
import com.kodebonek.instatraffic.art.ArtPagerAdapter;
import com.kodebonek.instatraffic.setting.SettingsActivity;
import com.kodebonek.instatraffic.utils.LocationData;
import com.kodebonek.instatraffic.utils.MyHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.OnReverseGeocodingListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationParams;
import io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesWithFallbackProvider;
import me.relex.circleindicator.CircleIndicator;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/*
project info:
left/right icon : 28dp, padding 4dp
shutter icon: 40dp, trim , padding 4dp ?
 */

public class MainActivity extends ActionBarActivity {

    private static final String TAG = "MainActivity";
    private static final String TAG_CAMERA_FRAGMENT = "CameraFragment" ;
    public static final int CAPTURE_MODE = 0;
    public static final int EDIT_MODE = 1;
    private static final int HQ_IMAGE_WIDTH = 1600;
    private static final int HQ_IMAGE_HEIGHT = 1600;
    private static final int SD_IMAGE_WIDTH = 800;
    private static final int SD_IMAGE_HEIGHT = 800;
    private static final int FLASH_AUTO = 0;
    private static final int FLASH_OFF = 1;
    private static final int FLASH_ON = 2;
    public static final String KEY_LOCATION_JSON = "location_json_string";
    public static final String KEY_CURRENT_MODE = "current_mode";
    public static final String KEY_USE_FFC = "use_ffc";
    public static final String KEY_FLASH_MODE = "flash_mode";
    public static final String KEY_SAVED_IMAGE_URI = "saved_image";
    public static final String KEY_LOCATION_DATA_PARCEL = "location_data";
    private static final String KEY_DISPLAYNAME = "display_name";
    public static final String KEY_SAVE_TO_PHONE = "save_to_phone";

    private MyCameraFragment cameraFragment = null;
    private ArtPagerAdapter artPagerAdapter = null;
    private ViewPager artPager = null;
    private boolean hasTwoCameras=(Camera.getNumberOfCameras() > 1);
    private SharedPreferences settings = null;

    private int currentMode = CAPTURE_MODE;
    private boolean firstTime = true;
    private Uri savedImageFileURI;
    private boolean mUseFFC = false;
    private int mFlashMode = FLASH_AUTO;
    private LocationData lastLocationData = null;
    private SmartLocation smartLocation;
    private int attemptCount;
    private boolean mSavedToPhone;
    private AsyncHttpClient httpClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        if(savedInstanceState != null) {
            Log.d(TAG,"onCreate savedInstanceState");
            currentMode = savedInstanceState.getInt(KEY_CURRENT_MODE);
            mUseFFC = savedInstanceState.getBoolean(KEY_USE_FFC);
            mFlashMode = savedInstanceState.getInt(KEY_FLASH_MODE);
            savedImageFileURI = savedInstanceState.getParcelable(KEY_SAVED_IMAGE_URI);
            lastLocationData = savedInstanceState.getParcelable(KEY_LOCATION_DATA_PARCEL);
            mSavedToPhone = savedInstanceState.getBoolean(KEY_SAVE_TO_PHONE);
            Log.d(TAG,"locdata from savedInstanceState: "+lastLocationData.toString());
        } else {
            //load settings ?
            String jsonString = settings.getString(KEY_LOCATION_JSON, "");
            if (jsonString.length()>0)
                lastLocationData = new LocationData(jsonString);
            else
                lastLocationData = new LocationData();
            Log.d(TAG,"locdata from beginning: "+lastLocationData.toString());
        }

        setContentView(R.layout.activity_main);

        Toolbar actionBar = (Toolbar) findViewById(R.id.toolbarTop);
        setSupportActionBar(actionBar);
/*
        ((TextView) findViewById(R.id.tvAppTitle1)).setTypeface(Typefaces.get(getApplicationContext(), "BebasNeue Bold"));
        ((TextView) findViewById(R.id.tvAppTitle2)).setTypeface(Typefaces.get(getApplicationContext(), "BebasNeue Bold"));
*/

        httpClient = new AsyncHttpClient();
        httpClient.setTimeout(30000);
        httpClient.setMaxRetriesAndTimeout(2,1000);

        //resize container to square format
        FrameLayout container = (FrameLayout) findViewById(R.id.container);
        int width = MyHelper.getDeviceWidth(this);
        container.setMinimumHeight(width);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width,width);    //square format
        container.setLayoutParams(lp);

        cameraFragment = MyCameraFragment.newInstance(MainActivity.this,mUseFFC);
        getFragmentManager().beginTransaction().replace(R.id.camera_container, cameraFragment, TAG_CAMERA_FRAGMENT).commit();

        Toolbar tbBottom =(Toolbar) findViewById(R.id.toolbarBottom);
        tbBottom.setContentInsetsAbsolute(0, 0);

        setupViewPager();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initButtons();
        showWatermark(false);

        setMode(MainActivity.CAPTURE_MODE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        locateMe();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (smartLocation!=null)
            smartLocation.location().stop();

        if (httpClient!=null)
            httpClient.cancelAllRequests(true);
    }

    private void initButtons() {

        ImageButton btnMenu = (ImageButton) findViewById(R.id.btnMenu);
        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(myIntent);
            }
        });
    }

    private void showWatermark(boolean showed) {
        LinearLayout watermark = (LinearLayout) findViewById(R.id.watermarkView);
        if (showed) watermark.setVisibility(View.VISIBLE);
        else watermark.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d(TAG,"onSaveInstanceState");

        outState.putInt(KEY_CURRENT_MODE, currentMode);
        outState.putBoolean(KEY_USE_FFC, mUseFFC);
        outState.putInt(KEY_FLASH_MODE, mFlashMode);
        outState.putParcelable(KEY_SAVED_IMAGE_URI, savedImageFileURI);
        outState.putParcelable(KEY_LOCATION_DATA_PARCEL, lastLocationData);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d(TAG, "onRestoreInstanceState");

        currentMode = savedInstanceState.getInt(KEY_CURRENT_MODE);
        mUseFFC = savedInstanceState.getBoolean(KEY_USE_FFC);
        mFlashMode = savedInstanceState.getInt(KEY_FLASH_MODE);
        savedImageFileURI = savedInstanceState.getParcelable(KEY_SAVED_IMAGE_URI);
        lastLocationData = savedInstanceState.getParcelable(KEY_LOCATION_DATA_PARCEL);
        mSavedToPhone = savedInstanceState.getBoolean(KEY_SAVE_TO_PHONE);

        super.onRestoreInstanceState(savedInstanceState);
    }

    public void setMode(int mode) {

        if (mode!=MainActivity.CAPTURE_MODE && mode!=MainActivity.EDIT_MODE)
            currentMode = MainActivity.CAPTURE_MODE;
        else
            currentMode = mode;

        ImageButton btnPrimary = (ImageButton) findViewById(R.id.button_primary);
        ImageButton btnLeft = (ImageButton) findViewById(R.id.button_left);
        ImageButton btnRight = (ImageButton) findViewById(R.id.button_right);
        ImageButton btnBack = (ImageButton) findViewById(R.id.btnBack);

        FrameLayout resultContainer = (FrameLayout) findViewById(R.id.image_container);
        FrameLayout cameraContainer = (FrameLayout) findViewById(R.id.camera_container);

        if (currentMode == MainActivity.CAPTURE_MODE) {

            if (!firstTime) {
                clearCaptureBitmap();

                //delete capture image if it not saved
                if (mSavedToPhone==false && savedImageFileURI!=null) {
                    Log.d(TAG,"deleting image : "+savedImageFileURI.getPath());
                    //getContentResolver().delete(savedImageFileURI,null,null);
                    File fdelete = new File(savedImageFileURI.getPath());
                    if (fdelete.exists()) {
                        fdelete.delete();
                        MediaScannerConnection.scanFile(this, new String[]{fdelete.toString()}, null, null);
                    }
                }
            } else
                firstTime = false;

            cameraFragment.restartPreview();
            savedImageFileURI = null;   //reset saved image
            mSavedToPhone = false;
            resultContainer.setVisibility(View.INVISIBLE);
            cameraContainer.setVisibility(View.VISIBLE);

            btnPrimary.setImageDrawable(getResources().getDrawable(R.drawable.ic_camera));
            btnPrimary.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    takePicture();
                }
            });

            btnBack.setVisibility(View.GONE);
            /*
            btnBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_near));
            btnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent myIntent = new Intent(MainActivity.this, TimelineActivity.class);
                    myIntent.putExtra("latitude", lastLocationData.latitude);
                    myIntent.putExtra("longitude", lastLocationData.longitude);
                    startActivity(myIntent);
                }
            });
            */

            updateFlashButton(mFlashMode);
            btnLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mFlashMode++;
                    if (mFlashMode > FLASH_ON) mFlashMode = FLASH_AUTO;   //rotate to first

                    updateFlashButton(mFlashMode);
                }
            });

            //switch camera button
            btnRight.setImageDrawable(getResources().getDrawable(R.drawable.ic_switch_camera));
            btnRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!hasTwoCameras) {
                        Toast.makeText(getApplicationContext(),"Sorry, your phone doesn't have a front camera",Toast.LENGTH_SHORT).show();
                    } else {
                        mUseFFC = !mUseFFC;
                        cameraFragment = MyCameraFragment.newInstance(MainActivity.this,mUseFFC);
                        getFragmentManager().beginTransaction().replace(R.id.camera_container, cameraFragment, TAG_CAMERA_FRAGMENT).commit();
                    }
                }
            });
        }
        else if (currentMode == MainActivity.EDIT_MODE) {

            resultContainer.setVisibility(View.VISIBLE);
            cameraContainer.setVisibility(View.INVISIBLE);

            btnPrimary.setImageDrawable(getResources().getDrawable(R.drawable.ic_share));
            btnPrimary.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showMyShareDialog();
                }
            });

            //back to camera button
            btnLeft.setImageDrawable(getResources().getDrawable(R.drawable.ic_camera_small));
            btnLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setMode(MainActivity.CAPTURE_MODE);
                }
            });

            //save button
            btnRight.setImageDrawable(getResources().getDrawable(R.drawable.ic_save_file));
            btnRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (saveCapturedImageToFile(true)) {
                        mSavedToPhone = true;
                    }
                }
            });

            btnBack.setVisibility(View.VISIBLE);
            btnBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
            btnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }

    private void showMyShareDialog() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.share_dialog, (ViewGroup) findViewById(R.id.share_dialog_layout));

        AlertDialog.Builder builder = new AlertDialog.Builder(this).setView(layout);
        final AlertDialog alertDialog = builder.create();
        //alertDialog.setTitle("Share Photo");

        Button btnUpload = (Button) layout.findViewById(R.id.btnUpload);
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //btnUpload.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.spin_animation, 0, 0);
                alertDialog.dismiss();
                showUploadDialog();
            }
        });
        ((ImageButton) layout.findViewById(R.id.btnFacebook)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (saveImageNoToast()==false)
                    return;

                Intent chooser = MyHelper.createChooserIntent(getApplicationContext(), "com.facebook.katana", "image/*", "InstaTraffic share", "Check this traffic! #instatraffic", savedImageFileURI);
                if (chooser!=null) {
                    alertDialog.dismiss();
                    startActivity(chooser);
                } else {
                    Toast.makeText(getApplicationContext(),"Can't find Facebook app on your phone.",Toast.LENGTH_SHORT).show();
                }
            }
        });
        ((ImageButton) layout.findViewById(R.id.btnTwitter)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (saveImageNoToast()==false)
                    return;

                Intent chooser = MyHelper.createChooserIntent(getApplicationContext(),"com.twitter.android","image/*","InstaTraffic share","Check this traffic! #instatraffic",savedImageFileURI);
                if (chooser!=null) {
                    alertDialog.dismiss();
                    startActivity(chooser);
                } else {
                    Toast.makeText(getApplicationContext(),"Can't find Twitter app on your phone.",Toast.LENGTH_SHORT).show();
                }
            }
        });
        ((ImageButton) layout.findViewById(R.id.btnInstagram)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (saveImageNoToast()==false)
                    return;

                Intent chooser = MyHelper.createChooserIntent(getApplicationContext(),"com.instagram.android","image/*","InstaTraffic share","Check this traffic! #instatraffic",savedImageFileURI);
                if (chooser!=null) {
                    alertDialog.dismiss();
                    startActivity(chooser);
                } else {
                    Toast.makeText(getApplicationContext(),"Can't find Instagram app on your phone.",Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button btnSave = (Button) layout.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //save and replace savedImageURI
                if (saveCapturedImageToFile(true)) {
                    mSavedToPhone = true;
                }
                alertDialog.dismiss();
            }
        });

        Button btnOther = (Button) layout.findViewById(R.id.btnOther);
        btnOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (saveImageNoToast()) {
                    alertDialog.dismiss();

                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    shareIntent.setType("image/*");
                    //shareIntent.putExtra(Intent.EXTRA_TEXT, "InstaMacet");
                    shareIntent.putExtra(Intent.EXTRA_STREAM, savedImageFileURI);
                    startActivity(Intent.createChooser(shareIntent, "Share to other"));
                }
            }
        });

        //finally
        alertDialog.show();

    }

    private void showUploadDialog() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.upload_dialog, (ViewGroup) findViewById(R.id.upload_dialog_layout));

        AlertDialog.Builder builder = new AlertDialog.Builder(this).setView(layout);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setTitle("Upload to InstaTraffic");
        String displayName = settings.getString(KEY_DISPLAYNAME, "");
        final EditText etName = (EditText) layout.findViewById(R.id.etName);
        etName.setText(displayName);

        Button btnUpload = (Button) layout.findViewById(R.id.btnUpload);
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (saveImageNoToast()) {
                    String name = etName.getText().toString();
                    String comment = ((EditText) layout.findViewById(R.id.etComment)).getText().toString();
                    //save current name
                    settings.edit().putString(KEY_DISPLAYNAME,name).commit();

                    uploadImageToServer(name, comment, savedImageFileURI, lastLocationData.latitude, lastLocationData.longitude);
                    alertDialog.dismiss();
                }
            }
        });

        alertDialog.show();
    }

    private void updateFlashButton(int mFlashMode) {

        ImageButton btnLeft = (ImageButton) findViewById(R.id.button_left);
        if (mFlashMode == FLASH_AUTO) {
            btnLeft.setImageDrawable(getResources().getDrawable(R.drawable.ic_flash_auto));
            cameraFragment.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
        } else if (mFlashMode == FLASH_OFF) {
            btnLeft.setImageDrawable(getResources().getDrawable(R.drawable.ic_flash_off));
            cameraFragment.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        } else if (mFlashMode == FLASH_ON) {
            btnLeft.setImageDrawable(getResources().getDrawable(R.drawable.ic_flash_on));
            cameraFragment.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
        }
    }

    private boolean saveImageNoToast() {
        if (savedImageFileURI == null)
            return(saveCapturedImageToFile(false));

        //already saved one
        return(true);
    }

    private boolean saveCapturedImageToFile(boolean showToast) {
        //add watermark to container
        FrameLayout container = (FrameLayout) findViewById(R.id.container);
        showWatermark(true);

        container.setDrawingCacheEnabled(true);
        container.buildDrawingCache();

        Bitmap cache;
        int hq = Integer.valueOf(settings.getString("photo_quality", "0"));
        if (hq==1) cache = MyHelper.getResizedBitmap(container.getDrawingCache(),HQ_IMAGE_WIDTH,HQ_IMAGE_HEIGHT);
        else cache = MyHelper.getResizedBitmap(container.getDrawingCache(),SD_IMAGE_WIDTH,SD_IMAGE_HEIGHT);

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMddHHmmss");
            Calendar cal = Calendar.getInstance();

            String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
            String appname = getResources().getString(R.string.app_name);
            File myDir = new File(root + "/"+ appname);
            myDir.mkdirs();

            String fname = "capture-" + dateFormat.format(cal.getTime()) + ".jpg";
            File file = new File(myDir, fname);

            FileOutputStream out = new FileOutputStream(file);
            cache.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

            savedImageFileURI = Uri.fromFile(file); //replace previous URI, it is possible coz user able to change Art Layout and re-save
            Log.d(TAG, "savedImageFileURI : " + savedImageFileURI);

            //save GPS to exif
            boolean dont_embed_gps = settings.getBoolean("dont_embed_gps",false);
            if (!dont_embed_gps) {
                MyHelper.saveCoordinatesToEXIF(savedImageFileURI,lastLocationData.latitude,lastLocationData.longitude);
                Log.d(TAG,"saved GPS info to image EXIF");
            }

            if (showToast)
                Toast.makeText(getApplicationContext(),"Photo saved!",Toast.LENGTH_SHORT).show();

            // Tell the media scanner about the new file so that it is immediately available to the user.
            MediaScannerConnection.scanFile(this, new String[]{file.toString()}, null, null);

        } catch (IOException e) {
            Log.e(TAG, "exception when save capture image: " + e.toString());
            Toast.makeText(getApplicationContext(),"save image failed: "+e.toString(),Toast.LENGTH_SHORT).show();
            showWatermark(false);
            return(false);
        } finally {
            showWatermark(false);
            container.destroyDrawingCache();
        }

        return(true);
    }

    private boolean isIntentCallable(Intent intent) {
        List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    private void setupViewPager() {
        artPagerAdapter = new ArtPagerAdapter(getSupportFragmentManager(),MainActivity.this);
        //artPagerAdapter.setContext(getApplicationContext());

        artPager = (ViewPager) findViewById(R.id.art_pager);
        artPager.setAdapter(artPagerAdapter);
        artPager.setOffscreenPageLimit(15); //art are cheap..lets load em all!!

        CircleIndicator pageIndicator = (CircleIndicator) findViewById(R.id.pager_indicator);
        pageIndicator.setViewPager(artPager);

        artPagerAdapter.notifyDataSetChanged();
    }

    private void showProgress(boolean show) {
        ProgressBar pb = (ProgressBar) findViewById(R.id.progressBar);
        TextView tvInfo = (TextView) findViewById(R.id.tvProgressInfo);
        if (show) {
            pb.bringToFront();
            pb.setVisibility(View.VISIBLE);

            tvInfo.bringToFront();
            tvInfo.setVisibility(View.VISIBLE);
        } else {
            pb.setVisibility(View.INVISIBLE);
            tvInfo.setVisibility(View.INVISIBLE);
        }
    }

    private void progressInfo(String info) {
        TextView tvInfo = (TextView) findViewById(R.id.tvProgressInfo);
        tvInfo.setText(info);
    }

    private void locateMe() {

        attemptCount = 1;
        showProgress(true);
        progressInfo("Acquiring your position ...");
        if (smartLocation==null)
            smartLocation = new SmartLocation.Builder(this).logging(true).build();

        //SmartLocation.with(this)
        smartLocation
                .location()
                .config(LocationParams.NAVIGATION)
                .provider(new LocationGooglePlayServicesWithFallbackProvider(this))
                //.oneFix()
                .start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {

                        Log.d(TAG, "onLocationUpdated: " + location.getLatitude() + "," + location.getLongitude() + " accuracy:" + location.getAccuracy() + "m provider:" + location.getProvider());
                        if (location.hasAccuracy() && location.getAccuracy() < 50) {
                            Log.d(TAG, "location matched! ");
                            lastLocationData.latitude = location.getLatitude();
                            lastLocationData.longitude = location.getLongitude();

                            smartLocation.location().stop();
                            progressInfo("Location found , finding the street address ..");
                            findNearestStreet(location.getLatitude(), location.getLongitude());

                        } else {
                            attemptCount++;
                            progressInfo("Still acquiring your location .. attempt #" + attemptCount);
                        }
                    }
                });
    }

    private void takePicture() {
        MyCameraFragment f = (MyCameraFragment) getFragmentManager().findFragmentByTag(TAG_CAMERA_FRAGMENT);
        if (f != null && f.isVisible()) {
            f.takeSimplePicture();
        } else {
            Toast.makeText(getApplicationContext(), "Take photo failed.", Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadImageToServer(String name,String comment,Uri imageFile,double latitude,double longitude) {

        if (imageFile==null) return;
        String URL = "http://api.kodebonek.com/instatraffic/upload.php";

        File f = new File(imageFile.getPath());
        if (f==null) {
            Toast.makeText(getApplicationContext(),"image not found?",Toast.LENGTH_SHORT).show();
            return;
        }

        Log.d(TAG,"uploading image "+f.getName());

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(60000);
        showProgress(true);
        RequestParams params = new RequestParams();
        try {
            params.put("latitude",latitude);
            params.put("longitude", longitude);
            params.put("displayname",name);
            params.put("comment",comment);
            params.put("picture", f , "image/jpg");
        } catch (FileNotFoundException e) {
            Log.e(TAG,e.toString());
        }
        client.post(URL, params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                showProgress(true);
                progressInfo("Uploading .. please wait");
                super.onStart();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                //super.onSuccess(statusCode, headers, response);
                Log.d(TAG, response.toString());
                Toast.makeText(getApplicationContext(), "Upload success", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                //super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d(TAG, "fail upload : " + throwable.toString());
                Toast.makeText(getApplicationContext(), "Upload failed: " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFinish() {
                showProgress(false);
                super.onFinish();
            }
        });
    }

    private void brodcastUpdateLocationToFragments(LocationData loc) {

        ArtData.getInstance().location = loc;

        List<Fragment> allFragments = getSupportFragmentManager().getFragments();
        if (allFragments == null || allFragments.isEmpty()) {
            Log.d(TAG, "getFragments return NULL or empty!!");
            return;
        }

        Log.d(TAG, "broadcast brodcastUpdateLocationToFragments to fragments");
        for (Fragment fragment : allFragments) {
            if (fragment!=null && !fragment.isAdded())
                continue;

            ((ArtFragment) fragment).updateInfo(loc);
        }

        //save it
        //artPagerAdapter.updateLocationData(loc);
    }

    public void broadcastUpdateToFragments() {
        List<Fragment> allFragments = getSupportFragmentManager().getFragments();
        if (allFragments == null || allFragments.isEmpty()) {
            Log.d(TAG, "getFragments return NULL or empty!!");
            return;
        }

        //Log.d(TAG, "broadcast to fragments");
        for (Fragment fragment : allFragments) {
            if (fragment != null && !fragment.isAdded())
                continue;

            ((ArtFragment) fragment).updateInfo();
        }
    }

    private void findNearestStreet(double latitude, double longitude) {
        if (httpClient==null) return;

        String URL = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+latitude+","+longitude;
        Log.d(TAG, URL);

        httpClient.get(URL, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                //Log.d(TAG,"response: "+response.toString());
                if (response.has("status")) {
                    String status = response.optString("status");
                    if (!status.equalsIgnoreCase("ok")) {
                        String error_message = response.optString("error_message");
                        Toast.makeText(getApplicationContext(), "Oops, we've problem getting address from Google. (" + status + "). Try again later",
                                Toast.LENGTH_LONG).show();
                        Log.e(TAG, "response error. Geocoder API: status=>" + status + " message=>" + error_message);
                        return;
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Oops, we've got invalid response from server. Try again later", Toast.LENGTH_LONG).show();
                    Log.e(TAG, "invalid api response. no 'status' field ?? dump: " + response.toString());
                    return;
                }

                try {
                    LocationData ld = lastLocationData;

                    String type, shortname, longname;
                    JSONArray addrComp = ((JSONArray) response.get("results")).getJSONObject(0).getJSONArray("address_components");
                    Log.d(TAG, "jSONArray: " + addrComp.toString());
                    for (int i = 0; i < addrComp.length(); i++) {

                        type = addrComp.getJSONObject(i).getJSONArray("types").getString(0);
                        shortname = addrComp.getJSONObject(i).getString("short_name");
                        longname = addrComp.getJSONObject(i).getString("long_name");

                        if (type.equalsIgnoreCase("route")) {
                            if (longname.length() - shortname.length() < 8)
                                ld.street = LocationData.getNormalizeStreetName(shortname);
                            else
                                ld.street = LocationData.getNormalizeStreetName(longname);

                        } else if (type.equalsIgnoreCase("country")) {
                            ld.country_code = shortname;
                            ld.country = LocationData.getNormalizeCountryName(longname);
                        } else if (type.equalsIgnoreCase("locality") || type.equalsIgnoreCase("administrative_area_level_2")) {
                            if (shortname.length() > 4) ld.city = LocationData.getNormalizeCityName(shortname);
                            else ld.city = LocationData.getNormalizeCityName(longname);
                        }

                        //handling if streetname still empty
                        if (ld.street.length() == 0 && type.contains("sublocality")) {
                            ld.street = LocationData.getNormalizeStreetName(longname);
                        }
                        if (ld.city.length() == 0 && (type.contains("administrative_area"))) {
                            ld.city = LocationData.getNormalizeCityName(longname);
                        }
                    }

                    //save data to last known
                    settings.edit().putString(KEY_LOCATION_JSON, ld.getJSONString()).commit();

                    Log.d(TAG, "result : " + ld.toString());
                    brodcastUpdateLocationToFragments(ld);

                } catch (JSONException e) {
                    Log.e(TAG, e.toString());
                    Toast.makeText(getApplicationContext(), "Problem when parsing server response. Try again later", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "Fail getting response from server. Try again later", Toast.LENGTH_LONG).show();
                Log.e(TAG, "onFailure : " + responseString);
            }

            @Override
            public void onFinish() {
                Log.d(TAG, "findNearestStreet finish.");
                showProgress(false);
                super.onFinish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (currentMode==MainActivity.EDIT_MODE) {
            setMode(MainActivity.CAPTURE_MODE);
        }
        else
            super.onBackPressed();
    }

    private void clearCaptureBitmap() {

        ImageView imageView = (ImageView)findViewById(R.id.image_result);
        Drawable drawable = imageView.getDrawable();
        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            Bitmap bitmap = bitmapDrawable.getBitmap();
            if (bitmap!=null) bitmap.recycle();
        }
        imageView.setImageDrawable(null);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
