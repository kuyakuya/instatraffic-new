package com.kodebonek.instatraffic;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;
import android.hardware.Camera.Size;

import com.commonsware.cwac.camera.CameraFragment;
import com.commonsware.cwac.camera.CameraView;
import com.commonsware.cwac.camera.PictureTransaction;
import com.commonsware.cwac.camera.SimpleCameraHost;

public class MyCameraFragment extends CameraFragment {

    private static final String KEY_USE_FFC = "USE_FFC";
    private static String TAG = "MyCameraFragment";
    private static MainActivity mContext;
    private boolean mIsProcessing = false;
    private int mImageWidth = 1000;
    private int mImageHeight = 1000;
    private CameraView mCameraView = null;

    public static MyCameraFragment newInstance(Context context,boolean useFFC) {
        mContext = (MainActivity) context;
        MyCameraFragment frag = new MyCameraFragment();

        Bundle args=new Bundle();
        args.putBoolean(KEY_USE_FFC, useFFC);
        frag.setArguments(args);

        return(frag);
    }

    public void setImageCaptureSize(int width,int height) {
        mImageWidth = width;
        mImageHeight = height;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SimpleCameraHost.Builder builder=
                new SimpleCameraHost.Builder(new MyCameraHost(getActivity()));

        setHost(builder.useFullBleedPreview(true).build());
    }

    private void setProcessing(boolean flag) {
        synchronized (this) {
            mIsProcessing = flag;
        }
    }

    private boolean isProcessing() {
        boolean is;
        synchronized (this) {
            is = mIsProcessing;
        }
        return(is);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mCameraView = (CameraView) super.onCreateView(inflater, container, savedInstanceState);

        mCameraView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //autoFocus();
            }
        });
        return (mCameraView);
    }

    @Override
    public void autoFocus() {
        if (mCameraView==null) return;

        if (isAutoFocusAvailable()) {
            super.autoFocus();
        }
    }

    @Override
    public void setFlashMode(String mode) {
        if (mCameraView==null) return;

        super.setFlashMode(mode);
    }

    public void takeSimplePicture() {
        if (mCameraView==null) return;

        if (isProcessing()) {
            //Toast.makeText(getActivity(),"camera not ready yet!",Toast.LENGTH_SHORT).show();
            return;
        }

        PictureTransaction pt = new PictureTransaction(getHost());
        pt.useSingleShotMode(true);
        pt.needBitmap(false);
        pt.needByteArray(true);

        setProcessing(true);
        takePicture(pt);
    }

    @Override
    public void restartPreview() {
        if (mCameraView==null) return;
        super.restartPreview();
    }

    class MyCameraHost extends SimpleCameraHost {

        public MyCameraHost(Context _ctxt) {
            super(_ctxt);
        }

        @Override
        public void saveImage(PictureTransaction xact, byte[] image) {

            BitmapFactory.Options options = new BitmapFactory.Options();
            //calculate the size first
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(image, 0, image.length, options);
            Log.d(TAG,"raw bitmap size. width:"+options.outWidth+" height:"+options.outHeight);

            //extract the bitmap
            options.inSampleSize = calculateInSampleSize(options, mImageWidth, mImageHeight);
            Log.d(TAG,"inSampleSize: "+options.inSampleSize);
            options.inJustDecodeBounds = false;
            final Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0, image.length,options);
            Log.d(TAG,"new bitmap size. width:"+bitmap.getWidth()+" height:"+bitmap.getHeight());

            Log.d(TAG,"saveImage");
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    FrameLayout fl = (FrameLayout) getActivity().findViewById(R.id.camera_container);
                    fl.setVisibility(View.INVISIBLE);

                    ImageView imageView = (ImageView) getActivity().findViewById(R.id.image_result);
                    imageView.setImageBitmap(bitmap);
                }
            });

            setProcessing(false);
        }

        private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
            // Raw height and width of image
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth) {

                final int halfHeight = height / 2;
                final int halfWidth = width / 2;

                // Calculate the largest inSampleSize value that is a power of 2 and keeps both
                // height and width larger than the requested height and width.
                while ((halfHeight / inSampleSize) > reqHeight
                        && (halfWidth / inSampleSize) > reqWidth) {
                    inSampleSize *= 2;
                }
            }

            return inSampleSize;
        }

        @Override
        protected boolean useFrontFacingCamera() {
            if (getArguments() == null) {
                return(false);
            }

            return(getArguments().getBoolean(KEY_USE_FFC));
        }

        @Override
        public boolean mirrorFFC() {
            //return super.mirrorFFC();
            return(true);
        }

        @Override
        public RecordingHint getRecordingHint() {
            return RecordingHint.STILL_ONLY;
        }

        @Override
        public void onCameraFail(FailureReason reason) {
            super.onCameraFail(reason);
            Toast.makeText(getActivity(),"Camera fail: "+reason.toString(),Toast.LENGTH_SHORT).show();
        }

        @Override
        public Camera.Parameters adjustPictureParameters(PictureTransaction xact, Camera.Parameters parameters) {
            parameters.setRotation(90);
            return super.adjustPictureParameters(xact, parameters);
        }

        @Override
        public Camera.Parameters adjustPreviewParameters(Camera.Parameters parameters) {
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);

            return super.adjustPreviewParameters(parameters);
        }

        @Override
        public Camera.ShutterCallback getShutterCallback() {
            //return super.getShutterCallback();
            return (new Camera.ShutterCallback() {
                @Override
                public void onShutter() {
                    Log.d(TAG, "onShutter");
                    //toggleUI();
                    ((MainActivity) mContext).setMode(MainActivity.EDIT_MODE);
                }
            });
        }

        @Override
        public Camera.Size getPictureSize(PictureTransaction xact, Camera.Parameters parameters) {
            return super.getPictureSize(xact, parameters);
        }

        @Override
        public Size getPreviewSize(int displayOrientation, int width, int height, Camera.Parameters parameters) {
            return super.getPreviewSize(displayOrientation, width, height, parameters);
       }
    }
}
