package com.kodebonek.instatraffic.utils;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/*
http://www.parcelabler.com/ --> parcelable generator
 */

public class LocationData implements Parcelable {

    private static final String TAG = "LocationData";
    public double latitude = 0;
    public double longitude = 0;
    public String street = "";
    public String city = "";
    public String province = "";
    public String country_code = "";
    public String country = "";

    public LocationData() { }

    public LocationData(String jsonString) {
        try {
            JSONObject obj = new JSONObject(jsonString);
            latitude = obj.optDouble("latitude");
            longitude = obj.optDouble("longitude");
            street = obj.optString("street");
            city = obj.optString("city");
            province = obj.optString("province");
            country_code = obj.optString("country_code");
            country = obj.optString("country");

        } catch (JSONException e) {
            Log.e(TAG,e.toString());
        }
    }

    public LocationData(Parcel source) {
        latitude = source.readDouble();
        longitude = source.readDouble();
        street = source.readString();
        city = source.readString();
        province = source.readString();
        country_code = source.readString();
        country = source.readString();
    }

    @Override
    public String toString() {
        return street+" "+city+" "+country+" ("+country_code+")";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(street);
        dest.writeString(city);
        dest.writeString(province);
        dest.writeString(country_code);
        dest.writeString(country);
    }

    public static Creator<LocationData> CREATOR = new Creator<LocationData>() {
        @Override
        public LocationData createFromParcel(Parcel source) {
            return new LocationData(source);
        }

        @Override
        public LocationData[] newArray(int size) {
            return new LocationData[size];
        }
    };

    public String getJSONString() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("latitude",latitude);
            obj.put("longitude",longitude);
            obj.put("street",street);
            obj.put("city",city);
            obj.put("province",province);
            obj.put("country_code",country_code);
            obj.put("country", country);

            return obj.toString();
        } catch (JSONException e) {
            Log.e(TAG, e.toString());
            return("");
        }
    }

    public static String getNormalizeStreetName(String in) {
        String name = in.replaceFirst("(?i)jalan","Jl.");
        name = name.replaceFirst(" (?i)street"," St");
        name = name.replaceFirst("(?i)avenida ","Av ");
        name = name.replaceFirst(" (?i)avenue "," Av");
        name = name.replaceFirst("(?i)rua ","R.");
        name = name.replaceFirst(" (?i)road"," Rd");
        return (name.trim());
    }

    public static String getNormalizeCityName(String in) {
        String name = in.replaceFirst("(?i)kota","");
        name = name.replaceFirst(" (?i)city","");
        return (name.trim());
    }

    public static String getNormalizeCountryName(String in) {
        String name = in.replaceFirst("(?i)republic of","");
        name = name.replaceFirst(" (?i)city","");
        return (name.trim());
    }
}
