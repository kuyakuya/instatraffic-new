package com.kodebonek.instatraffic.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;

import java.util.Hashtable;

/**
 * Created by bayufa on 5/14/2015.
 */
public class Typefaces{
    private static String TAG = "TypefacesHelper";
    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();

    public static Typeface get(Context c, String name){
        synchronized(cache){
            if(!cache.containsKey(name)){

                //try OTF first, then TTF
                Typeface t = Typeface.createFromAsset(c.getAssets(), String.format("fonts/%s.otf", name));
                if (t==null)
                    t = Typeface.createFromAsset(c.getAssets(), String.format("fonts/%s.ttf", name));

                if (t==null) {
                    Log.e(TAG, "typeface " + name + " not found in assets, use Default!!!");
                    return(Typeface.DEFAULT);
                }

                cache.put(name, t);
            }
            return cache.get(name);
        }
    }
}