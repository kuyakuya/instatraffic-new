package com.kodebonek.instatraffic.utils;

import android.content.Context;
import android.location.Address;
import android.util.Log;
import android.widget.Toast;

import com.kodebonek.instatraffic.utils.LocationData;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.List;

public class MyReverseGeocoder {

    static String TAG = "ReverseGeocoder";
    Context mContext;
    List<LocationData> resultData;
    private String API_USERNAME = "kodebonek";

    public MyReverseGeocoder(Context context)  {
        mContext = context;
    }

    Address address;

    public LocationData getLocationData(double latitude,double longitude) {
        LocationData ld = null;

        //Geocoder_findNearestStreet(latitude,longitude);

        return(ld);
    }

    private void Geocoder_findNearestStreet(double latitude, double longitude) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(60000);

        String URL = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+latitude+","+longitude;
        Log.d(TAG, URL);
        client.get(URL, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                //Log.d(TAG,"response: "+response.toString());
                if (response.has("status")) {
                    String status = response.optString("status");
                    if (!status.equalsIgnoreCase("ok")) {
                        Log.e(TAG,"response error. Geocoder API: status=>"+status+" message=>"+response.optString("error_message"));
                        return;
                    }
                } else {
                    Log.e(TAG,"invalid api response. no 'status' field ?? dump: "+response.toString());
                    return;
                }

                try {
                    LocationData ld = new LocationData();

                    String type,shortname,longname;
                    JSONArray addrComp = ((JSONArray)response.get("results")).getJSONObject(0).getJSONArray("address_components");
                    for (int i=0; i < addrComp.length(); i++) {

                        type = addrComp.getJSONObject(i).getJSONArray("types").getString(0);
                        shortname = addrComp.getJSONObject(i).getString("short_name");
                        longname = addrComp.getJSONObject(i).getString("long_name");
                        if (type.equalsIgnoreCase("route")) {
                            ld.street = longname;
                        } else if (type.equalsIgnoreCase("country")) {
                            ld.country_code = shortname;
                            ld.country = longname;
                        } else if (type.equalsIgnoreCase("administrative_area_level_2")) {
                            ld.city = shortname;
                        } else if (type.equalsIgnoreCase("locality")) {
                            ld.city = shortname;
                        }
                    }

                    Log.d(TAG,"result : "+ld.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                //super.onFailure(statusCode, headers, responseString, throwable);
                Log.e(TAG, "error get api : " + responseString);
            }
        });
    }

    private void GoogleDirection_findNearestStreet(double latitude,double longitude) {
        //http://maps.googleapis.com/maps/api/directions/json?origin=-6.917376,107.626805&destination=-6.910581,107.626526&sensor=false

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(60000);

        String URL = "http://maps.googleapis.com/maps/api/directions/json?origin="+latitude+","+longitude+"&destination="+latitude+","+longitude+"&sensor=false";
        Log.d(TAG, URL);
        client.get(URL, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                //Log.d(TAG,"response: "+response.toString());
                if (response.has("status")) {
                    String status = response.optString("status");
                    if (!status.equalsIgnoreCase("ok")) {
                        Log.e(TAG,"response error. Direction API: status=>"+status+" message=>"+response.optString("error_message"));
                        return;
                    }
                } else {
                    Log.e(TAG,"invalid api response. no 'status' field ?? dump: "+response.toString());
                    return;
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                //super.onFailure(statusCode, headers, responseString, throwable);
                Log.e(TAG, "error get api : " + responseString);
            }
        });
    }

    private void OSM_findNearestStreet(double latitude, double longitude) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(60000);

        String URL = "http://api.geonames.org/findNearbyStreetsOSMJSON?lat="+latitude+"&lng="+longitude+"&username="+API_USERNAME;
        Log.d(TAG, URL);
        client.get(URL, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                //super.onSuccess(statusCode, headers, response);
                Log.d(TAG,"response: "+response.toString());

                if (response.has("streetSegment")) {
                    JSONArray streetData = response.optJSONArray("streetSegment");

                    //parsing and remove duplicates data
                    HashSet<String> streetlist = new HashSet<String>();
                    for (int i=0; i < streetData.length(); i++) {
                        String street = streetData.optJSONObject(i).optString("name");
                        if (street.length()>0 && street!=null)
                            streetlist.add(street);
                    }

                    if (streetlist.size()>0)
                        Toast.makeText(mContext, streetlist.iterator().next(), Toast.LENGTH_SHORT).show();

                    Log.d(TAG,"nearest streets: "+streetlist.toString());

                } else {
                    Log.e(TAG,"received invalid json data!!");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                //super.onFailure(statusCode, headers, responseString, throwable);
                Log.e(TAG, "error get api : " + responseString);
            }
        });
    }


}
